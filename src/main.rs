

mod fibiter;

use notifica::notify;
use fibiter::FibIter;
use std::thread::sleep;
use std::time::Duration;

fn main() {
    let fib_gen = FibIter::new(Some(50));
    for (n, fib) in fib_gen {
        println!("nth: {}, fib: {}", n, fib);
        notify(&format!("the {}th fib", n), &format!("fib({}) = {}", n, fib));
        sleep(Duration::from_secs((fib * 60) as u64));
    };
}
