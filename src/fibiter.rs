
/// Fib generator
pub struct FibIter {
    /// a starts as 0 
    a: usize,
    /// b starts as 1
    b: usize,
    /// current_n is the index of the current a in the sequence
    current_n: usize,
    /// limit_n dictates a limit for the generator
    limit_n: Option<usize>
}

impl FibIter {
    pub fn new(limit_n: Option<usize>) -> Self {
        Self {
            a: 0, b: 1, current_n: 0, limit_n
        }
    }
}

impl Iterator for FibIter {
    /// Type of (current_n, a)
    type Item = (usize, usize);
    /// Returns the current index and the current fib in a tuple
    fn next(&mut self) -> Option<Self::Item> {
        match self.limit_n {
            Some(limit_n) => {
                if self.current_n >= limit_n {
                    return None;
                }
            },
            _ => {}
        }
        self.current_n += 1;
        let temp = self.b;
        self.b = self.a + self.b;
        self.a = temp;
        return Some((self.current_n, self.a));
    }
}
